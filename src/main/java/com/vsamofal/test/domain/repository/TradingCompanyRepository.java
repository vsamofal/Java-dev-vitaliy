package com.vsamofal.test.domain.repository;

import com.vsamofal.test.domain.TradingCompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TradingCompanyRepository extends JpaRepository<TradingCompanyEntity, Long> {
    TradingCompanyEntity findFirstBySymbolOrderByCreatedAt(String symbol);
}
