package com.vsamofal.test.domain.repository;

import com.vsamofal.test.domain.TradingCompanyDetailsEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TradingCompanyDetailsRepository extends PagingAndSortingRepository<TradingCompanyDetailsEntity, Long> {
    TradingCompanyDetailsEntity findFirstBySymbolOrderByCreatedAt(String symbol);

    @Query(value = "select * from (" +
            "SELECT e.*, max(e.created_at) over (partition by e.symbol) max_created_at " +
            "FROM trading_company_details_entity e " +
            "order by e.latest_volume desc, e.company_name asc) tcd1 " +
            "where tcd1.max_created_at = tcd1.created_at " +
            "limit 5"
            , nativeQuery = true)
    List<TradingCompanyDetailsEntity> topFiveRichestCompanies();

    @Query(value = "select * from (" +
            "SELECT e.*, max(e.created_at) over (partition by e.symbol) max_created_at " +
            "FROM trading_company_details_entity e " +
            "order by e.open_time desc, e.change_percent desc nulls last) tcd1 " +
            "where tcd1.max_created_at = tcd1.created_at " +
            "limit 5"
            , nativeQuery = true)
    List<TradingCompanyDetailsEntity> topFiveNewCompaniesWithTopChangePercent();
}
