package com.vsamofal.test.mapping;

import com.vsamofal.test.domain.TradingCompanyDetailsEntity;
import com.vsamofal.test.domain.TradingCompanyEntity;
import com.vsamofal.test.rest.vo.TradingCompany;
import com.vsamofal.test.rest.vo.TradingCompanyDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface TradingCompanyMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "createdAt", ignore = true)
    })
    TradingCompanyEntity mapTradingCompanyToEntity(TradingCompany tradingCompany);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "createdAt", ignore = true)
    })
    TradingCompanyDetailsEntity mapTradingCompanyDetailsToEntity(TradingCompanyDetails tradingCompanyDetails);

    TradingCompany mapTradingCompanyEntityToDTO(TradingCompanyEntity tradingCompany);

    TradingCompanyDetails mapTradingCompanyDetailsEntityToDTO(TradingCompanyDetailsEntity tradingCompanyDetails);


}
