package com.vsamofal.test.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "com.vsamofal.test.api")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiConfiguration {
    private String baseUrl;
    private String accessToken;
}
