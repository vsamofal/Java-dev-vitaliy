package com.vsamofal.test.rest.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradingCompanyDetails {
    private String symbol;
    private String companyName;
    private String calculationPrice;
    private Double open;
    private Long openTime;
    private Double close;
    private Long closeTime;
    private Double high;
    private Double low;
    private Double latestPrice;
    private String latestSource;
    private String latestTime;
    private Long latestUpdate;
    private Long latestVolume;
    private Double iexRealtimePrice;
    private String iexRealtimeSize;
    private String iexLastUpdated;
    private Double delayedPrice;
    private Long delayedPriceTime;
    private Double extendedPrice;
    private Double extendedChange;
    private Double extendedChangePercent;
    private Long extendedPriceTime;
    private Double previousClose;
    private Double change;
    private Double changePercent;
    private Double iexMarketPercent;
    private Long iexVolume;
    private Double avgTotalVolume;
    private Double iexBidPrice;
    private String iexBidSize;
    private Double iexAskPrice;
    private String iexAskSize;
    private Long marketCap;
    private String peRatio;
    private Double week52High;
    private Double week52Low;
    private Double ytdChange;
}
