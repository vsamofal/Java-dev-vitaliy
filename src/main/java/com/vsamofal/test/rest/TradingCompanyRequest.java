package com.vsamofal.test.rest;

import com.vsamofal.test.rest.vo.TradingCompany;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;

import java.util.List;

public class TradingCompanyRequest extends AbstractRequest<List<TradingCompany>, Void> {
    @Override
    public HttpMethod httpMethod() {
        return HttpMethod.GET;
    }

    @Override
    public ParameterizedTypeReference<List<TradingCompany>> responseClass() {
        return new ParameterizedTypeReference<List<TradingCompany>>() {
        };
    }

    @Override
    public String url() {
        return "ref-data/symbols";
    }

}
