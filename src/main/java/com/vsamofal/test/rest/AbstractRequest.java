package com.vsamofal.test.rest;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;

import java.util.Collections;
import java.util.Map;

public abstract class AbstractRequest<R, B> {

    public abstract HttpMethod httpMethod();

    public B getBody() {
//        body is not required
        return null;
    }

    public abstract ParameterizedTypeReference<R> responseClass();

    public abstract String url();

    public Map<String, String> requestParams() {
//        request params are not required
        return Collections.emptyMap();
    }

}
