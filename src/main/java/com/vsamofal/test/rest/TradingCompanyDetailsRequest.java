package com.vsamofal.test.rest;

import com.google.common.collect.ImmutableMap;
import com.vsamofal.test.rest.vo.TradingCompanyDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;

import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class TradingCompanyDetailsRequest extends AbstractRequest<TradingCompanyDetails, Void> {

    private static final String STOCK_CODE_KEY = "stockCode";

    private String stockCode;

    @Override
    public HttpMethod httpMethod() {
        return HttpMethod.GET;
    }

    @Override
    public ParameterizedTypeReference<TradingCompanyDetails> responseClass() {
        return new ParameterizedTypeReference<TradingCompanyDetails>() {
        };
    }

    @Override
    public String url() {
        return String.format("stock/{%s}/quote", STOCK_CODE_KEY);
    }

    @Override
    public Map<String, String> requestParams() {
        return ImmutableMap.of(STOCK_CODE_KEY, stockCode);
    }
}
