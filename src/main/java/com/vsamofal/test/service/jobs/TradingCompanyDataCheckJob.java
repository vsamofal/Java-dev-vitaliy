package com.vsamofal.test.service.jobs;

import com.vsamofal.test.rest.TradingCompanyDetailsRequest;
import com.vsamofal.test.rest.vo.TradingCompany;
import com.vsamofal.test.rest.vo.TradingCompanyDetails;
import com.vsamofal.test.service.RestClientService;
import com.vsamofal.test.service.TradingCompanyLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
@RequiredArgsConstructor
public class TradingCompanyDataCheckJob implements Callable<Boolean> {
    private final TradingCompanyLogService logService;
    private final RestClientService restClientService;

    private final TradingCompany tradingCompany;

    @Override
    public Boolean call() throws Exception {
        logService.saveTradingCompanyIfChanged(tradingCompany);
        TradingCompanyDetails details = restClientService.performTradingRequest(new TradingCompanyDetailsRequest(tradingCompany.getSymbol()));
        return logService.saveTradingCompanyDetailsIfChanged(details);
    }
}
