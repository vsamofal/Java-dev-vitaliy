package com.vsamofal.test.service;

import com.vsamofal.test.domain.TradingCompanyDetailsEntity;
import com.vsamofal.test.domain.repository.TradingCompanyDetailsRepository;
import com.vsamofal.test.rest.TradingCompanyRequest;
import com.vsamofal.test.rest.vo.TradingCompany;
import com.vsamofal.test.service.jobs.TradingCompanyDataCheckJob;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TradingCompanyJobService {

    private static final Pageable DEFAULT_PAGE_REQUEST = PageRequest.of(0, 5);

    private final TradingCompanyLogService logService;
    private final RestClientService restClientService;
    private final TradingCompanyDetailsRepository tradingCompanyDetailsRepository;

    private ExecutorService executorService;

    @Value("${com.vsamofal.test.parallel-requests}")
    private Integer parallelRequests;

    @PostConstruct
    public void init() {
        executorService = Executors.newFixedThreadPool(parallelRequests);
    }

    @Scheduled(fixedDelay = 5000)
    public void runJob() throws InterruptedException {
        long startTime = System.currentTimeMillis();
        log.info("Started cron job for checking trading companies data.");
        checkForNewCompanyData();
        log.info("Finished checking for trading companies data for {} ms", System.currentTimeMillis() - startTime);
        logStatistic();
    }


    private void checkForNewCompanyData() throws InterruptedException {
        List<TradingCompany> allCompanies = restClientService.performTradingRequest(new TradingCompanyRequest());
        List<TradingCompanyDataCheckJob> jobs = allCompanies.stream()
                .filter(TradingCompany::getIsEnabled)
                .map(tradingCompany -> new TradingCompanyDataCheckJob(logService, restClientService, tradingCompany))
                .collect(Collectors.toList());

        executorService.invokeAll(jobs);

    }

    private void logStatistic() {
        List<TradingCompanyDetailsEntity> topRichCompanies = tradingCompanyDetailsRepository.topFiveRichestCompanies();
        if (topRichCompanies.size() > 0) {
            log.info("--------Top Rich Companies--------");
            topRichCompanies.forEach(company -> log.info("Company {} - Volume {}", company.getCompanyName(), company.getLatestVolume()));
        }

        List<TradingCompanyDetailsEntity> topNewCompanies = tradingCompanyDetailsRepository.topFiveNewCompaniesWithTopChangePercent();

        if (topNewCompanies.size() > 0) {
            log.info("--------Top New Companies--------");
            topNewCompanies.forEach(company -> log.info("Company {} - Change Percent {}", company.getCompanyName(), company.getChangePercent()));
        }

    }


}
