package com.vsamofal.test.service;

import com.vsamofal.test.config.ApiConfiguration;
import com.vsamofal.test.rest.AbstractRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@RequiredArgsConstructor
public class RestClientService {

    private final RestTemplate restTemplate;
    private final ApiConfiguration apiConfiguration;


    @Retryable(
            maxAttempts = 5,
            backoff = @Backoff(delay = 1000, multiplier = 1.5))
    public <T, V> T performTradingRequest(AbstractRequest<T, V> request) {
        String requestUrl = buildTradingUrl(apiConfiguration.getBaseUrl(), apiConfiguration.getAccessToken(), request.url());

        HttpEntity<?> body = request.getBody() == null ? HttpEntity.EMPTY : new HttpEntity<>(request.getBody());

        ResponseEntity<T> response = restTemplate.exchange(requestUrl, request.httpMethod(), body, request.responseClass(), request.requestParams());
        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }

        throw new IllegalStateException(String.format("There is some http error, unable to perform requests, httpStatus: "
                , response.getStatusCode()));
    }

    private String buildTradingUrl(String baseUrl, String accessToken, String urlPart) {
        return String.format("%s/%s?token=%s", baseUrl, urlPart, accessToken);

    }
}
