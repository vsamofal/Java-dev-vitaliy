package com.vsamofal.test.service;

import com.vsamofal.test.domain.TradingCompanyDetailsEntity;
import com.vsamofal.test.domain.TradingCompanyEntity;
import com.vsamofal.test.domain.repository.TradingCompanyDetailsRepository;
import com.vsamofal.test.domain.repository.TradingCompanyRepository;
import com.vsamofal.test.mapping.TradingCompanyMapper;
import com.vsamofal.test.rest.vo.TradingCompany;
import com.vsamofal.test.rest.vo.TradingCompanyDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class TradingCompanyLogService {

    private final TradingCompanyMapper mapper;
    private final TradingCompanyRepository tradingCompanyRepository;
    private final TradingCompanyDetailsRepository tradingCompanyDetailsRepository;

    public boolean saveTradingCompanyIfChanged(@NotNull TradingCompany tradingCompany) {
        TradingCompany existTradingCompany = mapper.mapTradingCompanyEntityToDTO(
                tradingCompanyRepository.findFirstBySymbolOrderByCreatedAt(tradingCompany.getSymbol()));

        if (!tradingCompany.equals(existTradingCompany)) {
            TradingCompanyEntity tradingCompanyEntityForSave = mapper.mapTradingCompanyToEntity(tradingCompany);
            tradingCompanyRepository.save(tradingCompanyEntityForSave);
        }

        return false;
    }

    public boolean saveTradingCompanyDetailsIfChanged(@NotNull TradingCompanyDetails tradingCompanyDetails) {

        TradingCompanyDetails existTradingCompanyDetails = mapper.mapTradingCompanyDetailsEntityToDTO(
                tradingCompanyDetailsRepository.findFirstBySymbolOrderByCreatedAt(tradingCompanyDetails.getSymbol()));


        if (!tradingCompanyDetails.equals(existTradingCompanyDetails)) {
            TradingCompanyDetailsEntity tradingCompanyEntityForSave = mapper.mapTradingCompanyDetailsToEntity(tradingCompanyDetails);
            tradingCompanyDetailsRepository.save(tradingCompanyEntityForSave);
        }

        return false;
    }
}
